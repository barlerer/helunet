import pathlib
import random
from typing import List, Tuple

import pyrallis
import torch

from configurations.config import DatasetGeneration
from datasets.data import KappaDataGenerator
from linearoperator.linear_operator import HelmholtzOperatorTorch, ShiftedLaplacianOperatorTorch
from multigrid.multigrid import MultiGrid
from preconditioners.preconditioner import MultiGridPreconditioner
from solver.torch_fgmres import fgmres


def generate_data(path: pathlib.Path, samples: int, grid_size: int, frequency: int, gamma_value: float,
                  dataset_name: str, seed: int, dtype: torch.dtype, min_iter:int,max_iter:int,boundaries_pad: List[int],shifted_laplacian: bool):
    hight = width = grid_size
    f = frequency
    omega = 2 * torch.pi * f
    h = 2 / (hight + width)

    dataset = KappaDataGenerator(hight, width)
    dataset.load_data(data=dataset_name, seed=seed)
    for i in range(samples):
        gamma = gamma_value * omega * torch.ones(hight, width, dtype=dtype)
        kappa = dataset.generate_kappa().reshape(hight, width).to(dtype=dtype)
        helmholtz_operator = HelmholtzOperatorTorch(kappa, omega, gamma, h)
        sl_operator = ShiftedLaplacianOperatorTorch(kappa, omega, gamma, h)
        helmholtz_operator.absorbing_layer(boundaries_pad, omega)
        sl_operator.absorbing_layer(boundaries_pad, omega)
        # Generate true solution and RHS
        dtype_sol = torch.cfloat if dtype is torch.float32 else torch.cdouble
        x_sol = torch.randn(hight, width, dtype=dtype_sol)
        b = helmholtz_operator(x_sol).reshape_as(kappa)

        # Solve to get x_k
        mg = MultiGrid()
        M = MultiGridPreconditioner(sl_operator, mg)

        iterations = random.randint(min_iter, max_iter)
        x_k = fgmres(helmholtz_operator, b.flatten(), iterations, 1e-20, 1, precond=M, flexible=True).solution.reshape(
            hight, width)
        # Calculate residual and get the error
        error = (x_sol - x_k)
        residual = (b - helmholtz_operator(x_k)).reshape(hight, width) * (h ** 2)

        image = torch.stack((residual.real, residual.imag, kappa))
        error = torch.stack((error.real, error.imag))
        # Save in the data folder
        torch.save(image, path / f"dataimage{i}.pt")
        torch.save(error, path / f"error{i}.pt")


@pyrallis.wrap()
def main(cfg: DatasetGeneration):
    generate_data(path=cfg.output_path,
                  samples=cfg.samples,
                  dataset_name=cfg.dataset.dataset_name,
                  grid_size=cfg.grid,
                  frequency=cfg.frequency,
                  gamma_value=cfg.gamma,
                  seed=cfg.seed,
                  dtype=cfg.dtype,
                  min_iter=cfg.min_iter,
                  max_iter=cfg.max_iter,
                  shifted_laplacian=cfg.shifted_laplacian,
                  boundaries_pad=cfg.boundaries_pad)


if __name__ == "__main__":
    main()
