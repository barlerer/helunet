import torch
from torch import nn

from linearoperator.linear_operator import HelmholtzOperatorTorch


class MultiGridPreconditioner(nn.Module):
    def __init__(self, helmholtz_operator: HelmholtzOperatorTorch, mg):
        super().__init__()
        self.helmholtz_operator = helmholtz_operator
        self.hight, self.width = self.helmholtz_operator.kappa.shape
        self.mg = mg

    def forward(self, V):
        with torch.no_grad():
            return self.mg.vcycle(3, V.reshape(self.hight, self.width), self.helmholtz_operator).flatten()


class JacbiPreconditioner(nn.Module):
    def __init__(self, helmholtz_operator: HelmholtzOperatorTorch, mg, init_guess: torch.Tensor,):
        super().__init__()
        self.helmholtz_operator = helmholtz_operator
        self.hight, self.width = self.helmholtz_operator.kappa.shape
        self.mg = mg
        self.init_guess = init_guess

    def forward(self, b):
        with torch.no_grad():
            return self.mg.jacobi_relaxtion(10, b.reshape(self.hight, self.width), self.helmholtz_operator, self.init_guess, sl=True).flatten()


class NNPreconditioner(nn.Module):
    def __init__(self, helmholtz_operator: HelmholtzOperatorTorch, mg, model):
        super().__init__()
        self.helmholtz_operator = helmholtz_operator
        self.model = model
        self.mg = mg
        self.hight, self.width = helmholtz_operator.kappa.shape
        self.device = helmholtz_operator.kappa.device

    def forward(self, X):
        with torch.no_grad():
            # Reshape the residual onto a grid
            r = X.reshape(self.hight, self.width)
            # Split residual from complex form to channel representation
            R_split = torch.stack((r.real, r.imag))

            kappa = self.helmholtz_operator.kappa.unsqueeze(0)

            # Concatenate the residual and Kappa
            network_input = torch.concat((R_split, kappa)).unsqueeze(0)

            e = self.model(network_input).squeeze(0)
            e = torch.complex(e[0,], e[1,]).reshape(self.hight, self.width) * (
                self.helmholtz_operator.h**2
            )
            # Have Vcycle Smoothing
            e = self.mg.vcycle(
                levels=3, b=r, helmholtz_operator=self.helmholtz_operator, x=e.detach()
            )
            return e.flatten()