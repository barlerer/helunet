import pytorch_lightning as pl
import torch
from torch import nn
from typing import Any
from network.model_parts import DoubleConvolution, Down, HelBlock, ImplicitStep, OutConv, Up


class Encoder(pl.LightningModule):
    def __init__(self, n_channels: int, out_channels: int, channels_sizes: list[int], lr=0.001, *args, **kwargs):
        super().__init__()
        self.n_channels = n_channels
        self.out_channels = out_channels
        self.learning_rate = lr
        activation_function = kwargs.get("activation_function", "Softplus")
        self.inc = DoubleConvolution(
            n_channels, channels_sizes[0], activation_function=activation_function)
        self.downs = nn.ModuleList(
            [Down(channel, channel * 2) for channel in channels_sizes])
        self.save_hyperparameters()

    def forward(self, x):
        x = self.inc(x)
        down_features = [x]
        for down in self.downs:
            x = down(x)
            down_features.append(x)
        return down_features


class HelUnet(pl.LightningModule):
    def __init__(self, in_channels: int, out_channels: int, lr=1e-3, small=False,
                 *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.learning_rate = lr
        self.activation_function = kwargs.get("activation", "Softplus")
        self.save_hyperparameters()
        # Network layers
        self.downsample1 = HelBlock(self.in_channels, 8, 5, 2, padding=2)

        self.expand1 = HelBlock(8, 16, kernel_size=1)
        self.depth_wise1 = HelBlock(
            16, 16, kernel_size=3, groups=16, padding=1)
        self.shrink1 = HelBlock(16, 8, kernel_size=1, activation=True)

        self.downsample2 = HelBlock(8, 16, kernel_size=5, stride=2, padding=2)

        self.expand2 = HelBlock(16, 32, kernel_size=1)
        self.depth_wise2 = HelBlock(
            32, 32, kernel_size=3, groups=32, padding=1)
        self.shrink2 = HelBlock(32, 16, kernel_size=1, activation=True)

        self.downsample3 = HelBlock(16, 32, kernel_size=5, stride=2, padding=2)

        self.expand3 = HelBlock(32, 64, kernel_size=1)
        self.depth_wise3 = HelBlock(
            64, 64, kernel_size=3, groups=64, padding=1)
        self.shrink3 = HelBlock(64, 32, kernel_size=1, activation=True)

        if small:
            self.downsample4 = HelBlock(
                32, 64, kernel_size=5, stride=1, padding=2)
            self.up1 = Up(64, 32, self.activation_function, stride=1)
        else:
            self.downsample4 = HelBlock(
                32, 64, kernel_size=5, stride=2, padding=2)
            self.up1 = Up(64, 32, self.activation_function)

        self.implicit_step = ImplicitStep(1e-5, 64)
        self.up2 = Up(32, 16, self.activation_function)
        self.up3 = Up(16, 8, self.activation_function)
        self.out_conv = OutConv(8, self.out_channels)
        self.up_sample = nn.Upsample(scale_factor=2, mode="bilinear")

    def forward(self, x: torch.Tensor, features: list[torch.Tensor]) -> torch.Tensor:
        x1 = self.downsample1(x)

        x2 = self.expand1(x1)
        x3 = self.depth_wise1(x2) + features[1]
        x4 = self.shrink1(x3)

        x5 = self.downsample2(x4)

        x6 = self.expand2(x5)
        x7 = self.depth_wise2(x6) + features[2]
        x8 = self.shrink2(x7)

        x9 = self.downsample3(x8)

        x10 = self.expand3(x9)
        x11 = self.depth_wise3(x10) + features[3]
        x12 = self.shrink3(x11)

        x13 = self.downsample4(x12)
        x13 = self.implicit_step(x13)

        x = self.up1(x13, x9)
        x = self.up2(x, x5)
        x = self.up3(x, x1)
        x = self.out_conv(x)
        x = self.up_sample(x)
        return x


class UNet(pl.LightningModule):
    def __init__(self, in_channels: int, out_channels: int, lr=1e-3, small=False, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.learning_rate = lr
        self.activation_function = kwargs.get("activation", "Softplus")
        self.save_hyperparameters()
        # Network layers

        self.downsample1 = HelBlock(self.in_channels, 8, 5, 2, padding=2)

        self.expand1 = HelBlock(8, 16, kernel_size=1)
        self.depth_wise1 = HelBlock(
            16, 16, kernel_size=3, groups=16, padding=1)
        self.shrink1 = HelBlock(16, 8, kernel_size=1, activation=True)

        self.downsample2 = HelBlock(8, 16, kernel_size=5, stride=2, padding=2)

        self.expand2 = HelBlock(16, 32, kernel_size=1)
        self.depth_wise2 = HelBlock(
            32, 32, kernel_size=3, groups=32, padding=1)
        self.shrink2 = HelBlock(32, 16, kernel_size=1, activation=True)

        self.downsample3 = HelBlock(16, 32, kernel_size=5, stride=2, padding=2)

        self.expand3 = HelBlock(32, 64, kernel_size=1)
        self.depth_wise3 = HelBlock(
            64, 64, kernel_size=3, groups=64, padding=1)
        self.shrink3 = HelBlock(64, 32, kernel_size=1, activation=True)

        if small:
            self.downsample4 = HelBlock(
                32, 64, kernel_size=5, stride=1, padding=2)
            self.up1 = Up(64, 32, self.activation_function, stride=1)
        else:
            self.downsample4 = HelBlock(
                32, 64, kernel_size=5, stride=2, padding=2)
            self.up1 = Up(64, 32, self.activation_function)

        self.up2 = Up(32, 16, self.activation_function)
        self.up3 = Up(16, 8, self.activation_function)
        self.out_conv = OutConv(8, self.out_channels)
        self.up_sample = nn.Upsample(scale_factor=2, mode="bilinear")

    def forward(self, x: torch.Tensor, features: list[torch.Tensor]) -> torch.Tensor:
        x1 = self.downsample1(x)

        x2 = self.expand1(x1)
        x3 = self.depth_wise1(x2) + features[1]
        x4 = self.shrink1(x3)

        x5 = self.downsample2(x4)

        x6 = self.expand2(x5)
        x7 = self.depth_wise2(x6) + features[2]
        x8 = self.shrink2(x7)

        x9 = self.downsample3(x8)

        x10 = self.expand3(x9)
        x11 = self.depth_wise3(x10) + features[3]
        x12 = self.shrink3(x11)

        x13 = self.downsample4(x12)

        x = self.up1(x13, x9)
        x = self.up2(x, x5)
        x = self.up3(x, x1)
        x = self.out_conv(x)
        x = self.up_sample(x)
        return x


class HelcoderSolver(pl.LightningModule):
    def __init__(
            self, in_channels: int, out_channels: int, lr=1e-3, small=False, scheduler_steps=50, *args: Any,
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)
        encoder_channels = [8, 16, 32, 64, 128]
        self.encoder = Encoder(
            in_channels - 2, in_channels - 2, encoder_channels)
        self.solver = HelUnet(in_channels - 1,
                              out_channels,
                              small=small)
        self.lr = lr
        self.scheduler_steps = scheduler_steps
        self.save_hyperparameters()

    def forward(self, x) -> torch.Tensor:
        kappa = (x[:, 2]).unsqueeze(1)
        encoded_features = self.encoder(kappa)
        return self.solver(x[:, :2], encoded_features)

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop.
        x, y = batch
        loss = self.loss(x, y)
        self.log("train_loss", loss, prog_bar=True,
                 on_epoch=True, on_step=False)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        loss = self.loss(x, y)
        self.log("validation_loss", loss, prog_bar=True,
                 on_epoch=True, on_step=False)
        return loss

    def loss(self, x, y):
        x_hat = self(x)
        return nn.functional.mse_loss(x_hat, y)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, self.scheduler_steps)
        return {"optimizer": optimizer, "lr_scheduler": scheduler, "monitor": "validation_loss"}


class EncoderSolver(pl.LightningModule):
    def __init__(self, in_channels: int, out_channels: int, lr=1e-3, small=False, scheduler_steps=50, *args: Any,
                 **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        encoder_channels = [8, 16, 32, 64, 128]
        self.encoder = Encoder(
            in_channels - 2, in_channels - 2, encoder_channels)
        self.solver = UNet(in_channels - 1, out_channels, small=small)
        self.lr = lr
        self.scheduler_steps = scheduler_steps
        self.save_hyperparameters()

    def forward(self, x) -> torch.Tensor:
        kappa = (x[:, 2]).unsqueeze(1)
        encoded_features = self.encoder(kappa)
        return self.solver(x[:, :2], encoded_features)

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop.
        x, y = batch
        loss = self.loss(x, y)
        self.log("train_loss", loss, prog_bar=True,
                 on_epoch=True, on_step=False)
        return loss

    def validation_step(self, batch, batch_idx, dataloader_idx=0):
        x, y = batch
        loss = self.loss(x, y)
        self.log("validation_loss", loss, prog_bar=True,
                 on_epoch=True, on_step=False)
        return loss

    def loss(self, x, y):
        x_hat = self(x)
        return nn.functional.mse_loss(x_hat, y)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, self.scheduler_steps)
        return {"optimizer": optimizer, "lr_scheduler": scheduler}
