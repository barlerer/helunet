import torch
import torch.nn.functional as F
from torch import nn
import pytorch_lightning as pl

activation_factory = {"relu": nn.ReLU(),
                      "softplus": nn.Softplus()}


class DoubleConvolution(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, padding=1, activation_function="ReLU"):
        super().__init__()
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, padding=padding, bias=False),
            nn.BatchNorm2d(out_channels),
            activation_factory.get(activation_function.lower()),
            nn.Conv2d(out_channels, out_channels, kernel_size=kernel_size, padding=padding, bias=False),
            nn.BatchNorm2d(out_channels),
            activation_factory.get(activation_function.lower()),
        )

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    def __init__(self, in_channels, out_channels, activation_function="ReLU"):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.Conv2d(in_channels, in_channels, 3, stride=2, padding=1),
            DoubleConvolution(in_channels, out_channels, activation_function=activation_function)
        )

    def forward(self, x):
        return self.maxpool_conv(x)


class Up(nn.Module):
    def __init__(self, in_channels, out_channels, activation_function="ReLU", stride=2):
        super().__init__()
        self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=stride)
        self.conv = DoubleConvolution(in_channels, out_channels, activation_function=activation_function)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)


class ImplicitStep(pl.LightningModule):
    def __init__(self, h, c_in) -> None:
        super().__init__()
        self.h = h
        real = torch.tensor([[0, -1, 0], [-1, 4, -1], [0, -1, 0]])
        imag = torch.eye(3)
        self.weights = nn.Parameter(torch.stack((real, imag)).repeat(c_in // 2, 1, 1).reshape(1, c_in, 3, 3))

    def forward(self, r):
        m = self.weights.shape
        n = r.shape
        point_source = torch.zeros(n[-2], n[-1], dtype=self.dtype)
        point_source[n[-2] // 2, n[-1] // 2] = 1
        mid1 = (m[-1] - 1) // 2
        mid2 = (m[-2] - 1) // 2
        Kp = torch.zeros(m[1], 3*n[-2], 3*n[-1], dtype=self.dtype)
        Kp[:, :mid1 + 1, :mid2 + 1] = self.weights[:, :, mid1:, mid2:]
        Kp[:, -mid1:, :mid2 + 1] = self.weights[:, :, :mid1, -(mid2 + 1):]
        Kp[:, :mid1 + 1, -mid2:] = self.weights[:, :, -(mid1 + 1):, :mid2]
        Kp[:, -mid1:, -mid2:] = self.weights[:, :, :mid1, :mid2]
        k_hat = torch.fft.rfft2(Kp)

        b_pad = torch.zeros(3*n[-2], 3*n[-1], dtype=self.dtype)
        b_pad[n[-2]:n[-2]*2, n[-1]:n[-1]*2] = point_source
        b_hat = torch.fft.rfft2(b_pad)

        t = k_hat/((k_hat ** 2) + self.h)
        xKh = (b_hat * t)

        xKh = torch.fft.irfft2(xKh)[:, n[-2] // 2: 5*n[-2] // 2, n[-1] // 2: 5*n[-1] // 2]
        green = xKh

        width = r.shape[-1]  # b x c x H x W
        hight = r.shape[-2]  # b x c x H x W
        batch_size = r.shape[0]  # b x c x H x W
        channels = r.shape[1]
        green = torch.fft.fftshift(green).to(self.device)

        r_padded = torch.zeros(batch_size, channels, 2*hight, 2*width, device=self.device)
        r_padded[:, :, hight // 2: 3*hight // 2, width // 2: 3*width // 2] = r[:, :]
        # Transform the residual to its complex form
        u = torch.fft.irfft2(torch.fft.rfft2(green) * torch.fft.rfft2(r_padded))

        # Output should be b x 2 x H x W
        r = u[:, :, hight // 2: 3*hight // 2, width // 2: 3*width // 2]
        return r


class HelBlock(nn.Module):
    def __init__(self, in_channels: int, out_channels: int, kernel_size: int, stride: int = 1, groups: int = 1, padding: int = 0, activation=False, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if activation:
            self.conv = nn.Sequential(
                nn.Conv2d(in_channels, out_channels, kernel_size, stride, groups=groups, padding=padding),
                nn.BatchNorm2d(out_channels),
                nn.Softplus())
        else:
            self.conv = nn.Sequential(
                nn.Conv2d(in_channels, out_channels, kernel_size, stride, groups=groups, padding=padding),
                nn.BatchNorm2d(out_channels))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.conv(x)
