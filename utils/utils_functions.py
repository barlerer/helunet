import pathlib
import time
import warnings
from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
import pytorch_lightning as pl
import torch


def convergence_factor(x: torch.Tensor) -> float:
    x_0 = x[0]
    x_n = x <= x_0 * 1e-7
    T = x_n.nonzero()[0] if len(x_n.nonzero()) != 0 else torch.argmin(x)
    return (x[T] / x_0) ** (1.0 / T)


def plot_train_val_loss(train_loss, val_loss, path: Path, model_name: str):
    named_tuple = time.localtime()
    time_string = time.strftime("%d.%m.%Y,%H:%M:%S", named_tuple)

    if not path.exists():
        warnings.warn("Graph path does not exist, using current working directory")
        path = pathlib.Path.cwd()
    df = pd.DataFrame({"train_loss": train_loss})
    df.to_csv(path/f"{model_name}_train_loss_{time_string}.csv", index_label="epoch")
    df = pd.DataFrame({"val_loss": val_loss})
    df.to_csv(path/f"{model_name}_validation_loss_{time_string}.csv", index_label="epoch")

    plt.figure(figsize=(8, 8))
    plt.title('Training and Validation loss')
    plt.semilogy(train_loss, label='Train Loss')
    plt.semilogy(val_loss, label='Validation Loss')
    plt.xlabel('Epochs')
    plt.ylabel('MSE loss value')
    plt.legend()
    plt.savefig(f'{path}/{model_name}_{time_string}.png')


class MetricsCallback(pl.Callback):
    def __init__(self) -> None:
        super().__init__()
        self.train_loss = []
        self.val_loss = []

    def on_validation_epoch_end(self, trainer, pl_module):
        self.val_loss.append(trainer.logged_metrics['validation_loss'].item())

    def on_train_epoch_end(self, trainer, pl_module):
        self.train_loss.append(trainer.logged_metrics['train_loss'].item())