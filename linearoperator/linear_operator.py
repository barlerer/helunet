import torch
from torch import Tensor, nn

LAPLACE_KERNEL = torch.tensor([[0, -1, 0], [-1, 4, -1], [0, -1, 0]]).unsqueeze(0).unsqueeze(0)


class HelmholtzOperatorTorch(nn.Module):
    """
    Helmholtz operator class.
    """

    def __init__(self, kappa: torch.Tensor, omega: float, gamma: torch.Tensor, h: float) -> None:
        self.kappa = kappa
        self.omega = omega
        self.gamma = gamma
        self.h = h
        self.device = kappa.device
        super().__init__()

    def generate_helmholtz_matrix(self):
        return ((self.kappa ** 2) * self.omega) * (self.omega - self.gamma * 1j)

    def generate_shifted_laplacian(self, alpha=0.5):
        return ((self.kappa ** 2) * self.omega) * ((self.omega - self.gamma * 1j) - (1j * self.omega * alpha))

    def matmat(self, X: torch.Tensor, h: float, operator=None) -> torch.Tensor:
        original_shape = X.shape
        if (X.dim() == 1):
            X = X.reshape_as(self.kappa)
        if (X.dim() == 2):
            X = X.unsqueeze(0).unsqueeze(0)
        convolution = self._convolve_2d(X, 1 / (h ** 2) * LAPLACE_KERNEL)
        if operator == 'SL':
            return (convolution - self.generate_shifted_laplacian() * X).reshape(original_shape)
        return (convolution - self.generate_helmholtz_matrix() * X).reshape(original_shape)

    def matvec(self, X: torch.Tensor, h):
        return self.matmat(X.reshape_as(self.kappa), h)

    def forward(self, x, operator: str = ''):
        if operator == 'SL':
            return self.matmat(x, self.h, operator)
        return self.matmat(x, self.h)

    def absorbing_layer(self, pad, ABLamp, NeumannAtFirstDim=False):
        self.gamma = self._absorbing_layer(self.gamma, pad, ABLamp, NeumannAtFirstDim)

    def _absorbing_layer(self, gamma, pad, ABLamp, NeumannAtFirstDim=False):
        device = gamma.device
        n = gamma.shape
        b_bwd1 = ((torch.arange(pad[0], 0, -1) ** 2) / (pad[0] ** 2)).reshape(-1, 1).to(device)
        b_bwd2 = ((torch.arange(pad[1], 0, -1) ** 2) / (pad[1] ** 2)).reshape(-1, 1).to(device)

        b_fwd1 = ((torch.arange(1, pad[0] + 1) ** 2) / (pad[0] ** 2)).reshape(-1, 1).to(device)
        b_fwd2 = ((torch.arange(1, pad[1] + 1) ** 2) / (pad[1] ** 2)).reshape(-1, 1).to(device)

        I1 = torch.arange(n[0] - pad[0], n[0], device=device)
        I2 = torch.arange(n[1] - pad[1], n[1], device=device)

        if not NeumannAtFirstDim:
            gamma[:, :pad[1]] += torch.ones(n[0], 1, device=device) @ b_bwd2.T * ABLamp
            gamma[:pad[0], :pad[1]] -= (b_bwd1 @ b_bwd2.T) * ABLamp
            gamma[I1, :pad[1]] -= b_fwd1 @ b_bwd2.T * ABLamp

        gamma[:, I2] += (torch.ones(n[0], 1, device=device) @ b_fwd2.T) * ABLamp
        gamma[:pad[0], :] += (b_bwd1 @ torch.ones(1, n[1], device=device)) * ABLamp
        gamma[I1, :] += (b_fwd1 @ torch.ones(1, n[1], device=device)) * ABLamp
        gamma[:pad[0], I2] -= (b_bwd1 @ b_fwd2.T) * ABLamp
        gamma[I1[0]:I1[-1] + 1, I2[0]:I2[-1] + 1] -= (b_fwd1 @ b_fwd2.T) * ABLamp

        return gamma

    def _convolve_2d(self, x: torch.Tensor, k: torch.Tensor):
        """Convolves x with k

        Parameters
        ----------
        x : torch.Tensor
            input, should be in dimensions of bxcxHxW
        k : torch.Tensor
            kernel,should be in dimensions of bxcxHxW

        Returns
        -------
        ndarray
            x convolved with k
        """
        return torch.nn.functional.conv2d(x, k.to(x.device, x.dtype), padding=1)


class ShiftedLaplacianOperatorTorch(HelmholtzOperatorTorch):
    def __init__(self, kappa: Tensor, omega: float, gamma: Tensor, h: float) -> None:
        super().__init__(kappa, omega, gamma, h)

    def forward(self, x, operator: str = ''):
        return super().forward(x, operator="SL")
