import pyrallis
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
from pytorch_lightning.loggers import WandbLogger

from configurations.config import TrainConfig
from datasets.data import DataModule
from utils.utils_functions import (MetricsCallback, plot_train_val_loss)
from pathlib import Path


def train_network(exp_name: str, model: pl.LightningModule, dataset_path: str | list[str], graph_path: Path,
                  val_percent: float, batch_size: int, epochs: int, workers: int, seed: int, accelerator="auto",
                  tags=None) -> pl.LightningModule:
    # Generate WandB logger
    wandb_logger = WandbLogger(project="HelUNet", name=exp_name, log_model="all", tags=tags)
    wandb_logger.watch(model)
    dm = DataModule(dataset_path, val_percent, seed, batch_size, workers)
    metrics = MetricsCallback()
    mntr_ckpt = ModelCheckpoint(monitor="train_loss")
    lr_monitor = LearningRateMonitor("epoch")
    callbacks = [metrics, mntr_ckpt, lr_monitor]
    # 5. Train the network
    trainer = pl.Trainer(max_epochs=epochs, callbacks=callbacks, accelerator=accelerator,
                         logger=wandb_logger, reload_dataloaders_every_n_epochs=1)
    trainer.fit(model, datamodule=dm)
    # 6. Plot the loss of validation and train
    train_loss, val_loss = metrics.train_loss, metrics.val_loss
    # Discard val_loss first value, as it is before any training is performed
    val_loss.pop(0)
    plot_train_val_loss(train_loss, val_loss, graph_path, exp_name)
    return model


@pyrallis.wrap()
def main(cfg: TrainConfig) -> None:
    pl.seed_everything(cfg.seed)
    print(cfg)

    # Decides on which data to evaluate
    model = train_network(cfg.exp_name,
                          cfg.architecture.model,
                          cfg.dataset_path,
                          cfg.graph_path,
                          cfg.val_percent,
                          cfg.batch_size,
                          cfg.epochs,
                          cfg.workers,
                          cfg.seed,
                          tags=cfg.tags,
                          accelerator=cfg.device)


if __name__ == "__main__":
    main()
