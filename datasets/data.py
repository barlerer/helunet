import os
import random

import torch
import torchvision
import torchvision.transforms as transforms
from pytorch_lightning.utilities.types import TRAIN_DATALOADERS, EVAL_DATALOADERS
from torch.utils.data import Dataset, random_split, DataLoader
import numpy as np
import pytorch_lightning as pl


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2 ** 32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


g = torch.Generator()
g.manual_seed(0)


class KappaDataGenerator:
    def __init__(self, hight: int, width: int) -> None:
        self.trainloader = None
        self.dataset = None
        self.hight = hight
        self.width = width
        self.iter = None

    def load_data(self, batch_size: int = 1, data="stl10", train=True, custom_dataset=None, seed=42):
        torch.manual_seed(seed)
        g = torch.Generator()
        if data == "stl10":
            transform = transforms.Compose(
                [
                    transforms.ToTensor(),
                    transforms.Grayscale(1),
                    transforms.Resize((self.hight, self.width), antialias=True),
                    transforms.GaussianBlur(5),
                    MinMaxScalerVectorized(feature_range=(0.25, 1)),
                ]
            )
            split = "train" if train else "test"
            dataset = torchvision.datasets.STL10(
                root="./data", split=split, download=True, transform=transform
            )

        elif data == "cifar10":
            transform = transforms.Compose(
                [
                    transforms.ToTensor(),
                    transforms.GaussianBlur(5, 6),
                    transforms.Grayscale(1),
                    transforms.Resize((self.hight, self.width), antialias=True),
                    MinMaxScalerVectorized(feature_range=(0.25, 1)),
                ]
            )
            dataset = torchvision.datasets.CIFAR10(
                root="./data", train=train, download=True, transform=transform
            )
        elif data == "openfwi":
            transform = transforms.Compose([transforms.ToTensor(), transforms.Resize(
                (self.hight, self.width), antialias=True), MinMaxScalerVectorized(feature_range=(0.25, 1))])
            dataset = OpenFWIDataset("data/stylea", 500, transform)
        else:
            dataset = custom_dataset

        self.trainloader = torch.utils.data.DataLoader(
            dataset, batch_size=batch_size, shuffle=False, worker_init_fn=seed_worker, generator=g
        )
        self.dataset = dataset
        self.iter = iter(self.trainloader)

    def generate_kappa(self) -> torch.Tensor:
        image, _ = next(self.iter)
        return image


class MinMaxScalerVectorized:
    """MinMax Scaler

    Transforms each channel to the range [a, b].

    Parameters
    ----------
    feature_range : tuple
        Desired range of transformed data.
    """

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __call__(self, tensor):
        """Fit features

        Parameters
        ----------
        stacked_features : tuple, list
            List of stacked features.

        Returns
        -------
        tensor
            A tensor with scaled features using requested preprocessor.
        """
        # Feature range
        a, b = self.feature_range
        return (tensor - tensor.min()) * (b - a) / (tensor.max() - tensor.min()) + a


class OpenFWIDataset(Dataset):
    def __init__(
            self,
            folder_path: str,
            samples_per_file: int,
            transform=None,
            target_transform=None,
            preload=True,
    ) -> None:
        super().__init__()
        self.folder_path = folder_path
        self.transform = transform
        self.target_transform = target_transform
        self.preload = preload
        self.samples_per_file = samples_per_file
        self.samples = []
        self.prefix_data = "model"

        if self.preload:
            file_names = sorted(
                [
                    name
                    for name in os.listdir(self.folder_path)
                    if os.path.isfile(os.path.join(self.folder_path, name))
                       and name.startswith(self.prefix_data)
                ]
            )
            self.samples = [
                np.load(f"{self.folder_path}/{file_name}") for file_name in file_names
            ]

    def __getitem__(self, index) -> torch.Tensor:
        batch_idx, sample_idx = (
            index // self.samples_per_file,
            index % self.samples_per_file,
        )
        if self.preload:
            data = self.samples[batch_idx][sample_idx]
        else:
            data = np.load(f"{self.folder_path}/{self.prefix_data}{index}.npy")
            data = data[sample_idx]

        if self.transform:
            data = data.transpose(1, 2, 0)
            data = self.transform(data)

        return data, data

    def __len__(self) -> int:
        return self.samples_per_file * self._get_num_of_batches()

    def _get_num_of_batches(self):
        return len(
            [
                name
                for name in os.listdir(self.folder_path)
                if os.path.isfile(os.path.join(self.folder_path, name))
                   and name.startswith(self.prefix_data)
            ]
        )


class DataModule(pl.LightningDataModule):
    def __init__(self, dataset_path: str | list[str], val_percent: float, seed: int, batch_size: int,
                 workers: int) -> None:
        super().__init__()
        self.val_loaders = []
        self.train_loaders = []
        self.batch_size = batch_size
        self.val_percent = val_percent
        self.seed = seed
        self.workers = workers
        self.dataset_path = dataset_path if type(dataset_path) == list else [dataset_path]
        self.curr_dataloader_idx = 0

    def setup(self, stage: str = "") -> None:
        datasets = [RandomKappaDataset(dataset)
                    for dataset in self.dataset_path]
        for dataset in datasets:
            n_val = int(len(dataset) * self.val_percent)
            n_train = len(dataset) - n_val
            train_set, val_set = random_split(
                dataset, [n_train, n_val], generator=torch.Generator().manual_seed(self.seed))

            self.train_loaders.append(train_set)
            self.val_loaders.append(val_set)

    def train_dataloader(self) -> TRAIN_DATALOADERS:
        if self.trainer.current_epoch % 20 == 0 and self.trainer.current_epoch!=0:
            self.curr_dataloader_idx = (self.curr_dataloader_idx + 1) % len(self.train_loaders)
            print("Switched train_loader")
        return DataLoader(self.train_loaders[self.curr_dataloader_idx], batch_size=self.batch_size,
                          num_workers=self.workers)

    def val_dataloader(self) -> EVAL_DATALOADERS:
        if self.trainer.current_epoch % 20 == 0 and self.trainer.current_epoch!=0:
            self.curr_dataloader_idx = (self.curr_dataloader_idx + 1) % len(self.val_loaders)
            print("Switched val_loader")
        return DataLoader(self.val_loaders[self.curr_dataloader_idx], batch_size=self.batch_size,
                          num_workers=self.workers)


PREFIX_DATA = "dataimage"
PREFIX_LABEL = "error"


class RandomKappaDataset(Dataset):
    def __init__(self, folder_path, transform=None, target_transform=None) -> None:
        self.transform = transform
        self.target_transform = target_transform
        self.folder_path = folder_path

    def __len__(self):
        return len([name for name in os.listdir(self.folder_path)
                    if os.path.isfile(os.path.join(self.folder_path, name)) and name.startswith(PREFIX_DATA)])

    def __getitem__(self, index):
        image = torch.load(f"{self.folder_path}/{PREFIX_DATA}{str(index)}.pt")
        label = torch.load(f"{self.folder_path}/{PREFIX_LABEL}{str(index)}.pt")

        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label
