import numpy as np
import pyrallis
import pytorch_lightning as pl
import torch

from configurations.config import EvaluationConfig, Dataset
from datasets.data import KappaDataGenerator, MinMaxScalerVectorized, OpenFWIDataset
from linearoperator.linear_operator import HelmholtzOperatorTorch
from multigrid.multigrid import MultiGrid
from preconditioners.preconditioner import MultiGridPreconditioner, NNPreconditioner
from solver.torch_fgmres import fgmres
from network.models import UNet, HelUnet, HelcoderSolver
import wandb


@torch.no_grad()
def solve(helmholtz_operator, preconditioner, b: torch.Tensor, iterations, max_restarts=10, tol=1e-7):
    x_sol = fgmres(helmholtz_operator, b.flatten(), max_restarts=max_restarts, rel_tol=tol,
                   max_iter=iterations, precond=preconditioner, flexible=True)
    residuals = x_sol.residual_norms

    return x_sol.solution, torch.tensor(residuals)


@torch.no_grad()
def single_point(helmholtz_operator, preconditioner, iterations=100, max_restarts=10, tol=1e-7):
    kappa = helmholtz_operator.kappa
    hight, width = [*kappa.shape]
    b = torch.zeros_like(kappa, dtype=torch.cfloat)
    b[hight // 2, width // 2] = 1 / ((helmholtz_operator.h) ** 2)
    return solve(helmholtz_operator, preconditioner, b, iterations, max_restarts, tol=tol)


@torch.no_grad()
def random_rhs(helmholtz_operator, preconditioner, iterations=100, max_restarts=10, tol=1e-7):
    kappa = helmholtz_operator.kappa
    b = torch.rand_like(kappa, dtype=torch.cfloat)
    return solve(helmholtz_operator, preconditioner, b, iterations, max_restarts, tol=tol)


@pyrallis.wrap(config_path="eval_config.yaml")
def main(cfg: EvaluationConfig):
    evaluation_title = cfg.title
    run = wandb.init(name=evaluation_title, config={
        "trained_upto": cfg.top_train_size, "dataset": cfg.kappa_dataset.dataset_name}, tags=cfg.tags)
    print(evaluation_title)
    artifact1 = run.use_artifact(cfg.model_artifact, type='model')
    artifact_dir1 = artifact1.download()

    # Edit this to match your own model location
    model_path = f"{artifact_dir1}/model.ckpt"
    device = torch.device(cfg.device)
    model = cfg.network.model.__class__

    model = model.load_from_checkpoint(checkpoint_path=model_path,
                                       map_location=device,
                                       in_channels=cfg.network.input_channels,
                                       out_channels=cfg.network.output_channels,
                                       small=cfg.network.small).to(device).eval()

    # Iterations test
    with torch.no_grad():
        mg = MultiGrid()
        grids = cfg.grids
        frequencies = cfg.frequencies
        gamma_value = cfg.gamma
        for i, grid in enumerate(grids):
            hight = width = grid
            f = frequencies[i]
            omega = 2 * torch.pi * f
            h = 2.0 / (hight + width)

            kappa_gen = KappaDataGenerator(hight, width)
            kappa_gen.load_data(data=cfg.kappa_dataset.dataset_name,
                                train=False,
                                seed=cfg.kappa_dataset.seed)

            iterations = []
            for _ in range(cfg.samples):
                gamma = gamma_value * omega * torch.ones(hight, width, device=device)
                kappa = kappa_gen.generate_kappa().reshape(hight, width).to(device)

                helmholtz_operator = HelmholtzOperatorTorch(kappa, omega, gamma, h)
                helmholtz_operator.absorbing_layer(cfg.boundaries_pad, omega)

                precond = NNPreconditioner(helmholtz_operator, mg, model)
                if cfg.point_source:
                    # Run experiment for single-source
                    _, unet_residuals = single_point(helmholtz_operator, precond)
                else:
                    # Run experiment for random rhs
                    _, unet_residuals = random_rhs(helmholtz_operator, precond)

                iterations.append(len(unet_residuals))

            grid_res = f"Grid {grid} average {np.mean(iterations)}"
            print(grid_res)
            run.log({f"{grid}_iterations": grid_res})


if __name__ == '__main__':
    main()
