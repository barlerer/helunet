import pathlib
from dataclasses import dataclass
from typing import Union, List, Optional, Tuple
import pyrallis
import torch.utils.data
from pyrallis import field
import pytorch_lightning as pl
from network.models import (
    EncoderSolver,
    HelcoderSolver,
    UNet,
)


@pyrallis.decode.register(torch.dtype)
def decode_array(x):
    x = x.lower()
    if x in ["torch.float32", "float32"]:
        return torch.float32
    elif x in ["torch.float64", "float64"]:
        return torch.float64


@dataclass
class NeuralNetwork:
    """Training config for the experiment. Will dictate the architecture used."""
    # Architecture to select. Options are: HelcoderSolver, EncoderSolver, UNet
    architecture: str = field(default="UNet")
    # Input channels
    input_channels: int = field(default=3)
    # Output channels
    output_channels: int = field(default=2)
    # Indicating whether small architecture will be used (Meaning the last layers will include stride=2)
    small: bool = field(default=False)
    # Scheduler steps
    scheduler_steps: int = field(default=50)
    # dtype, should be consistent through the experiment.
    dtype: torch.dtype = field(default=torch.float32)
    # Learning rate (Relevant only to training, not needed for inference)
    lr: float = field(default=1e-3)

    @property
    def model(self) -> pl.LightningModule:
        factory = {
            "UNet": UNet(self.input_channels, self.output_channels, small=self.small, lr=self.lr),
            "EncoderSolver": EncoderSolver(self.input_channels,
                                           self.output_channels,
                                           small=self.small,
                                           scheduler_steps=self.scheduler_steps,
                                           lr=self.lr),
            "HelcoderSolver": HelcoderSolver(self.input_channels,
                                             self.output_channels,
                                             small=self.small,
                                             scheduler_steps=self.scheduler_steps, lr=self.lr),
        }
        return factory[self.architecture].to(self.dtype)


@dataclass
class Dataset:
    """Dataset config"""
    # Dataset to create. Options are OpenFWI,STL10, CIFAR10 or custom
    dataset_name: str = field(default="CIFAR10")
    # Dataset path, required for OpenFWI and custom
    dataset_path: Optional[str] = field(default="")
    # Seed used for dataloader
    seed: int = field(default=42)

    def __post_init__(self):
        self.dataset_name = self.dataset_name.lower()
        if self.dataset_name in ["openfwi", "custom"]:
            assert self.dataset_path
        assert self.dataset_name in ["stl10", "cifar10", "custom", "openfwi"]


@dataclass
class TrainConfig:
    """Training config for the experiment."""
    # Dataset path. The path provided must be of a dataset generated using the generate_data.py. If provided as a list,
    # will replace dataset every 20 epochs.
    dataset_path: Union[list, str] = field()
    # Path where graphs will be saved
    graph_path: pathlib.Path = field(default=pathlib.Path)
    # The experiment name, will be used in WandB
    exp_name: str = field(default="default_exp")
    # Batch size for training and validation
    batch_size: int = field(default=32)
    # Value of percent for train\validation split
    val_percent: float = field(default=0.2)
    # Seed for reproducibility
    seed: int = field(default=42)
    # Torch device, can be 'cpu' or 'cuda'. 'mps' is not supported because complex numbers are used
    device: str = field(default="cpu")
    # Number of epochs to train the network
    epochs: int = field(default=100)
    # Number of PyTorch Lightning workers (CPU cores)
    workers: int = field(default=4)
    # Architecture to train
    architecture: NeuralNetwork = field(default_factory=NeuralNetwork)
    # Training tags (For logging)
    tags: List[str] = field(default_factory=list)


@dataclass
class EvaluationConfig:
    """Evaluation config"""
    # Dataset path
    title: str = field()
    # Gamma value
    gamma: float = field()
    # FGMRES Iterations
    iterations: int = field()
    # FGMRES restarts
    max_restarts: int = field()
    # Model artifact (From wandb)
    model_artifact: str = field()
    # Grids sizes
    grids: List[int] = field(default_factory=list)
    # Frequencies size
    frequencies: List[int] = field(default_factory=list)
    # FGMRES tolerance
    tolerance: float = field(default=1e-7)
    # Torch device, can be 'cpu' or 'cuda'
    device: str = field(default="cpu")
    # Model to evaluate
    network: NeuralNetwork = field(default_factory=NeuralNetwork)
    # Maximum size of training samples (For logging)
    top_train_size: Optional[int] = field(default=None)
    # Dataset used for eval
    kappa_dataset: Dataset = field(default_factory=Dataset)
    # Evaluation tags (For logging)
    tags: List[str] = field(default_factory=list)
    # Boundaries padding size
    boundaries_pad: List[int] = field(default_factory=lambda: [16, 16])
    # Number of samples to average the results (Per grid size)
    samples: int = field(default=1)
    # torch dtype, can be torch.float32 or torch.float64. Should be consistent throughout the experiment
    dtype: torch.dtype = field(default=torch.float64)
    # Right-hand-side as point source. If false, RHS will be random
    point_source: bool = field(default=True)

    def __post_init__(self):
        assert len(self.grids) == len(self.frequencies)
        assert len(self.boundaries_pad) == 2


@dataclass
class DatasetGeneration:
    """Evaluation config"""
    # Dataset path
    samples: int = field(default=1)
    # Gamma value
    gamma: float = field(default=0.05)
    # Grid size
    grid: int = field(default=128)
    # Frequency size
    frequency: int = field(default=10)
    # Output path
    output_path: pathlib.Path = field(default=pathlib.Path.cwd())
    # Dataset used for data generation
    dataset: Dataset = field(default_factory=Dataset)
    # Boundaries padding size
    boundaries_pad: List[int] = field(default_factory=lambda: [16, 16])
    # Seed for reproducibility
    seed: int = field(default=42)
    # torch dtype, can be torch.float32 or torch.float64.
    dtype: torch.dtype = field(default=torch.float64)
    # Minimum number of iterations
    min_iter: int = field(default=2)
    # Maximum number of iterations
    max_iter: int = field(default=20)
