import torch
import torch.nn.functional as F

from linearoperator.linear_operator import (HelmholtzOperatorTorch, ShiftedLaplacianOperatorTorch)
from solver.torch_fgmres import fgmres

DOWN_KERNEL = torch.tensor([[1, 2, 1], [2, 4, 2], [1, 2, 1]]
                           ).unsqueeze(0).unsqueeze(0) * 1./16
UP_KERNEL = torch.tensor([[1, 2, 1], [2, 4, 2], [1, 2, 1]]
                         ).unsqueeze(0).unsqueeze(0) * 1./4


class MultiGrid:

    def jacobi_relaxtion(self, iterations: int, b: torch.Tensor, helmholtz_operator: HelmholtzOperatorTorch, x: torch.Tensor = None, w: float = 0.8, sl=False) -> torch.Tensor:
        kappa = helmholtz_operator.kappa
        h = helmholtz_operator.h
        if x is None:
            x = torch.zeros_like(kappa, dtype=torch.cfloat)
        for _ in range(iterations):
            if sl:
                y = helmholtz_operator(x, 'SL').reshape_as(kappa)
                d = 4/(h ** 2) - helmholtz_operator.generate_shifted_laplacian()
            else:
                y = helmholtz_operator(x).reshape_as(kappa)
                d = 4/(h ** 2) - helmholtz_operator.generate_helmholtz_matrix()

            residual = b - y
            alpha = w / d
            x += alpha*residual
        return x

    def down(self, matrix: torch.Tensor):
        """We apply 2D convolution, input should be of size HxW

        Parameters
        ----------
        matrix : torch.Tensor
            The matrix we want to down-convolve
        """
        return F.conv2d(matrix.unsqueeze(0).unsqueeze(0), DOWN_KERNEL.to(device=matrix.device, dtype=matrix.dtype), stride=2, padding=1).squeeze(0).squeeze(0)

    def up(self, matrix: torch.Tensor):
        """Computes upsample of the given matrix, with UP_Kernel. 
        Parameters
        ----------
        matrix : torch.Tensor
            The matrix which we want to upsample

        Returns
        -------
        torch.Tensor
            The upsampled matrix
        """
        return F.conv_transpose2d(matrix.unsqueeze(0).unsqueeze(0), UP_KERNEL.to(matrix.device, matrix.dtype), stride=2, padding=1, output_padding=1).squeeze(0).squeeze(0)

    def vcycle(self, levels: int, b: torch.Tensor, helmholtz_operator: HelmholtzOperatorTorch, x: torch.Tensor = None, gmres_max_itr: int = 20, gmres_max_restart=1):

        if x is None:
            x = torch.zeros_like(helmholtz_operator.kappa, dtype=torch.cfloat)
        if levels == 0:
            kappa = helmholtz_operator.kappa
            omega = helmholtz_operator.omega
            gamma = helmholtz_operator.gamma
            h = helmholtz_operator.h

            # Coarsest grid
            # # FGMRES here
            shifted_laplacian_operator = ShiftedLaplacianOperatorTorch(kappa, omega, gamma, h)
            tol = 1e-10
            original_shape = x.shape
            x = fgmres(shifted_laplacian_operator, b.flatten(), max_iter=gmres_max_itr, x0=x.flatten(), max_restarts=gmres_max_restart, rel_tol=tol)
            return x.solution.reshape(original_shape)
        else:
            kappa = helmholtz_operator.kappa
            omega = helmholtz_operator.omega
            gamma = helmholtz_operator.gamma
            h = helmholtz_operator.h

            # Smooth the solution
            x = self.jacobi_relaxtion(1, b, helmholtz_operator, x, sl=True)
            # We need to get to a finer grid
            residual = b - helmholtz_operator(x).reshape_as(kappa)

            # Get our parameter values for lower grid
            residual_coarse = self.down(residual)
            kappa_coarse = self.down(kappa)
            gamma_coarse = self.down(gamma)
            helmholtz_operator_coarse = HelmholtzOperatorTorch(kappa_coarse, omega, gamma_coarse, h*2)

            x_coarse = self.vcycle(levels - 1, residual_coarse,
                                   helmholtz_operator_coarse, None, gmres_max_itr)
            # Delete tensors to free memory
            del residual_coarse
            del kappa_coarse
            del gamma_coarse
            fine_error = self.up(x_coarse)
            x += fine_error
            del fine_error
            del x_coarse
            del helmholtz_operator_coarse
            x = self.jacobi_relaxtion(1, b, helmholtz_operator, x, sl=True)
        return x
